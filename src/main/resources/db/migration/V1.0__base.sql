CREATE SEQUENCE author_id_seq INCREMENT BY 1 MINVALUE 0 NO MAXVALUE START WITH 1;
CREATE TABLE author (
  id bigint NOT NULL DEFAULT nextval('author_id_seq'),
  name varchar(200) NOT NULL,
  PRIMARY KEY(id)
);

CREATE SEQUENCE book_id_seq INCREMENT BY 1 MINVALUE 0 NO MAXVALUE START WITH 1;
CREATE TABLE book (
  id bigint NOT NULL DEFAULT nextval('book_id_seq'),
  name varchar(200) NOT NULL,
  author_id bigint NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (author_id) REFERENCES author(id)
);
