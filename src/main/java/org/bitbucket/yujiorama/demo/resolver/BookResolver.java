package org.bitbucket.yujiorama.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.bitbucket.yujiorama.demo.repository.AuthorRepository;
import org.bitbucket.yujiorama.demo.type.Author;
import org.bitbucket.yujiorama.demo.type.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class BookResolver implements GraphQLResolver<Book> {
    private AuthorRepository authorRepository;

    public Author author(Book book) {
        return authorRepository.findById(book.getAuthorId()).get();
    }
}
