package org.bitbucket.yujiorama.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.bitbucket.yujiorama.demo.repository.BookRepository;
import org.bitbucket.yujiorama.demo.type.Author;
import org.bitbucket.yujiorama.demo.type.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthorResolver implements GraphQLResolver<Author> {
    private BookRepository bookRepository;

    public Iterable<Book> books(Author author) {
        return bookRepository.getBooksByAuthorId(author.getId());
    }
}
