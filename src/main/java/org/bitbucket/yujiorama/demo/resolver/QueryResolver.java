package org.bitbucket.yujiorama.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.bitbucket.yujiorama.demo.repository.AuthorRepository;
import org.bitbucket.yujiorama.demo.repository.BookRepository;
import org.bitbucket.yujiorama.demo.type.Author;
import org.bitbucket.yujiorama.demo.type.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    public Iterable<Author> authors() {
        return authorRepository.findAll();
    }

    public Iterable<Book> books() {
        return bookRepository.findAll();
    }

    public Author getAuthorById(Integer id) {
        return authorRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Author(authorId=" + id + ") not found"));
    }
}
