package org.bitbucket.yujiorama.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.bitbucket.yujiorama.demo.repository.AuthorRepository;
import org.bitbucket.yujiorama.demo.repository.BookRepository;
import org.bitbucket.yujiorama.demo.type.Author;
import org.bitbucket.yujiorama.demo.type.Book;
import org.bitbucket.yujiorama.demo.type.CreateAuthorInput;
import org.bitbucket.yujiorama.demo.type.CreateBookInput;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {
    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    public Author createAuthor(CreateAuthorInput input) {
        return authorRepository.save(
            Author.builder()
                .name(input.getName())
                .build()
        );
    }

    public Book createBook(CreateBookInput input) {
        if (!authorRepository.existsById(input.getAuthorId())) {
            throw new IllegalArgumentException("Author(authorId=" + input.getAuthorId() + ") not found");
        }
        return bookRepository.save(
            Book.builder()
                .authorId(input.getAuthorId())
                .name(input.getName())
                .build()
        );
    }
}
