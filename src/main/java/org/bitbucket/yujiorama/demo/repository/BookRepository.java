package org.bitbucket.yujiorama.demo.repository;

import org.bitbucket.yujiorama.demo.type.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {
    Iterable<Book> getBooksByAuthorId(Integer id);
}
