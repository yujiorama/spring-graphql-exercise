package org.bitbucket.yujiorama.demo.repository;

import org.bitbucket.yujiorama.demo.type.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> {
}
