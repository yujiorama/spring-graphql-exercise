package org.bitbucket.yujiorama.demo;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import graphql.servlet.ObjectMapperConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringGraphqlExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGraphqlExerciseApplication.class, args);
	}

}
