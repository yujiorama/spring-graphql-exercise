INSERT INTO author(name) values('foo');
INSERT INTO author(name) values('bar');
INSERT INTO author(name) values('baz');
INSERT INTO author(name) values('xyz');
INSERT INTO book(name, author_id)
SELECT
'foo first book',
id as author_id
from author where name='foo';
INSERT INTO book(name, author_id)
SELECT
  'foo second book',
  id as author_id
from author where name='foo';
INSERT INTO book(name, author_id)
SELECT
  'bar first book',
  id as author_id
from author where name='bar';
INSERT INTO book(name, author_id)
SELECT
  'baz first book',
  id as author_id
from author where name='baz';
INSERT INTO book(name, author_id)
SELECT
  'xyz first book',
  id as author_id
from author where name='xyz';
