package org.bitbucket.yujiorama.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.PostgreSQLContainer;

import java.time.Duration;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles(profiles = {"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringGraphqlExerciseApplicationTests {

    private static PostgreSQLContainer POSTGRES;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeAll
    static void before() {
        POSTGRES = new PostgreSQLContainer<>()
            .withStartupTimeout(Duration.ofSeconds(600));
        POSTGRES.start();
        assertTrue(POSTGRES.isRunning());
    }

    @AfterAll
    static void after() {
        POSTGRES.stop();
        assertFalse(POSTGRES.isRunning());
    }

    @Test
    public void testQueryAuthors() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String queryBody = "{ \"query\": \"{ authors { id, name, books { id, name } } }\" }";
        HttpEntity<String> request = new HttpEntity<>(queryBody, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("/graphql", request, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonNode data = mapper.readTree(response.getBody()).get("data");
        JsonNode authors = data.get("authors");
        assertTrue(authors.isArray());
        for (Iterator<JsonNode> iter = authors.elements(); iter.hasNext(); ) {
            JsonNode author = iter.next();
            assertFalse(author.get("id").textValue().isEmpty());
            assertFalse(author.get("name").textValue().isEmpty());
            assertTrue(author.get("books").isArray());
        }
    }

    @Test
    public void testQueryBooks() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String queryBody = "{ \"query\": \"{ books { id, name } }\" }";
        HttpEntity<String> request = new HttpEntity<>(queryBody, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("/graphql", request, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonNode data = mapper.readTree(response.getBody()).get("data");
        JsonNode books = data.get("books");
        assertTrue(books.isArray());
        for (Iterator<JsonNode> iter = books.elements(); iter.hasNext(); ) {
            JsonNode book = iter.next();
            assertFalse(book.get("id").textValue().isEmpty());
            assertFalse(book.get("name").textValue().isEmpty());
        }
    }

    @Test
    public void testMutateCreateAuthor() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String queryBody = "{ \"query\": \"mutation { createAuthor(input: { name: \\\"test\\\" }) { id, name, books { id, name } } }\" }";
        HttpEntity<String> request = new HttpEntity<>(queryBody, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("/graphql", request, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonNode data = mapper.readTree(response.getBody()).get("data");
        JsonNode createAuthor = data.get("createAuthor");
        assertFalse(createAuthor.isArray());
        assertFalse(createAuthor.get("id").textValue().isEmpty());
        assertFalse(createAuthor.get("name").textValue().isEmpty());
        assertTrue(createAuthor.get("books").isArray());
    }

    @Test
    public void testMutateCreateBook() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String queryBody = "{ \"query\": \"mutation { createBook(input: { authorId: 1, name: \\\"test\\\" }) { id, name } }\" }";
        HttpEntity<String> request = new HttpEntity<>(queryBody, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("/graphql", request, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonNode data = mapper.readTree(response.getBody()).get("data");
        JsonNode createBook = data.get("createBook");
        assertFalse(createBook.isArray());
        assertFalse(createBook.get("id").textValue().isEmpty());
        assertFalse(createBook.get("name").textValue().isEmpty());
    }
}

