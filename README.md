spring-graphql-exercise
====

## links

* [GraphQL Java Kickstart](https://www.graphql-java-kickstart.com/)
* [Schemas and Types](https://graphql.org/learn/schema/#the-query-and-mutation-types)
* [45.3.5 Testing with a running server](https://docs.spring.io/spring-boot/docs/2.1.x/reference/html/boot-features-testing.html)
* [Usage - Testcontainers](https://www.testcontainers.org/usage.html#junit)
* [Temporary database containers - Testcontainers](https://www.testcontainers.org/usage/database_containers.html#jdbc-url)

## step

* create project
* change pom.xml
* add resources
* run test

### create project.

```bash
mkdir -p spring-graphql-exercise
spring init \
  --force \
  --dependencies=web,data-jpa,postgresql,devtools,flyway,lombok \
  --artifactId=spring-graphql-exercise \
  --groupId=org.bitbucket.yujiorama.demo \
  --name=spring-graphql-exercise \
  --type=maven-project \
  spring-graphql-exercise
```

### change pom.xml.

```xml
+   <description>Spring Boot + GraphQL-Java-Kickstart + Test Containers + JUnit 5 Demo project for Spring Boot</description>
+   <url>https://bitbucket.org/yujiorama/spring-graphql-exercise.git</url>

+   <scm>
+     <url>https://bitbucket.org/yujiorama/spring-graphql-exercise.git</url>
+   </scm>

+   <licenses>
+     <license>
+       <name>MIT</name>
+       <url>https://bitbucket.org/yujiorama/spring-graphql-exercise/src/master/LICENSE.txt</url>
+       <distribution>repo</distribution>
+     </license>
+   </licenses>

    <properties>
        <java.version>1.8</java.version>
+        <kotlin.version>1.3.10</kotlin.version>
+        <graphql-spring-boot-starter.version>5.2</graphql-spring-boot-starter.version>
+        <graphql-java-tools.version>5.4.1</graphql-java-tools.version>
+        <junit-jupiter.version>5.3.2</junit-jupiter.version>
+        <junit-platform.version>1.3.0</junit-platform.version>
    </properties>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
+            <exclusions>
+                <exclusion>
+                    <groupId>org.springframework.boot</groupId>
+                    <artifactId>spring-boot-starter-tomcat</artifactId>
+                </exclusion>
+            </exclusions>
        </dependency>
+        <dependency>
+            <groupId>org.springframework.boot</groupId>
+            <artifactId>spring-boot-starter-undertow</artifactId>
+        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
+            <exclusions>
+                <exclusion>
+                    <groupId>junit</groupId>
+                    <artifactId>junit</artifactId>
+                </exclusion>
+            </exclusions>
        </dependency>

+        <dependency>
+            <groupId>org.junit.jupiter</groupId>
+            <artifactId>junit-jupiter-api</artifactId>
+            <scope>test</scope>
+        </dependency>
+        <dependency>
+            <groupId>org.junit.jupiter</groupId>
+            <artifactId>junit-jupiter-engine</artifactId>
+            <scope>test</scope>
+        </dependency>
+        <dependency>
+            <groupId>org.testcontainers</groupId>
+            <artifactId>postgresql</artifactId>
+            <version>${testcontainers.version}</version>
+            <scope>test</scope>
+        </dependency>

+        <dependency>
+            <groupId>com.graphql-java-kickstart</groupId>
+            <artifactId>graphql-spring-boot-starter</artifactId>
+            <version>${graphql-spring-boot-starter.version}</version>
+        </dependency>
+        <dependency>
+            <groupId>com.graphql-java-kickstart</groupId>
+            <artifactId>graphiql-spring-boot-starter</artifactId>
+            <version>${graphql-spring-boot-starter.version}</version>
+        </dependency>
+        <dependency>
+            <groupId>com.graphql-java-kickstart</groupId>
+            <artifactId>voyager-spring-boot-starter</artifactId>
+            <version>${graphql-spring-boot-starter.version}</version>
+        </dependency>
+        <dependency>
+            <groupId>com.graphql-java-kickstart</groupId>
+            <artifactId>graphql-java-tools</artifactId>
+            <version>${graphql-java-tools.version}</version>
+        </dependency>
+        <dependency>
+            <groupId>com.graphql-java-kickstart</groupId>
+            <artifactId>graphql-spring-boot-starter-test</artifactId>
+            <version>${graphql-spring-boot-starter.version}</version>
+            <scope>test</scope>
+        </dependency>

```

### add resources.

```
./src/test/resources/application-test.yml
./src/test/resources/db/migration/V999.0__data.sql
./src/test/java/org/bitbucket/yujiorama/demo/SpringGraphqlExerciseApplicationTests.java
./src/main/resources/graphql/mutation.graphqls
./src/main/resources/graphql/query.graphqls
./src/main/resources/db/migration/V1.1__data.sql
./src/main/resources/db/migration/V1.0__base.sql
./src/main/resources/application.yml
./src/main/java/org/bitbucket/yujiorama/demo/repository/AuthorRepository.java
./src/main/java/org/bitbucket/yujiorama/demo/repository/BookRepository.java
./src/main/java/org/bitbucket/yujiorama/demo/type/Author.java
./src/main/java/org/bitbucket/yujiorama/demo/type/Book.java
./src/main/java/org/bitbucket/yujiorama/demo/type/CreateAuthorInput.java
./src/main/java/org/bitbucket/yujiorama/demo/type/CreateBookInput.java
./src/main/java/org/bitbucket/yujiorama/demo/SpringGraphqlExerciseApplication.java
./src/main/java/org/bitbucket/yujiorama/demo/resolver/MutationResolver.java
./src/main/java/org/bitbucket/yujiorama/demo/resolver/BookResolver.java
./src/main/java/org/bitbucket/yujiorama/demo/resolver/AuthorResolver.java
./src/main/java/org/bitbucket/yujiorama/demo/resolver/QueryResolver.java
```

### run test.

```bash
./mvnw clean test
```
